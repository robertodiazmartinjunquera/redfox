# RESUMEN

<p>Este es un script para eliminar la versión snap del navegador firefox en Ubuntu 22.04 LTS e instalar la versión tradicional en formato deb atraves del PPA de Mozilla Security.</p>

# INSTRUCCIONES DE EJECUCIÓN

```bash
chmox +x redfox.sh              #para dar permisos de ejecución al script
```

```bash
./redfox.sh                     #para ejecutar el script
```
