#!/bin/bash
echo
echo "Hola, bienvenido a redfox"
echo
echo "Con este script vamos a eliminar la versión snap de firefox en tu Ubuntu e instalaremos la versión tradicional en formato DEB."
echo
echo "Empezemos, es posible que tengas que escribir tu contraseña de usuario"
sleep 2s
echo
sudo snap remove firefox
echo
sudo add-apt-repository ppa:mozillateam/ppa -y
echo
sudo apt update
echo
sudo apt install -t 'o=LP-PPA-mozillateam' firefox firefox-locale-es
echo
echo "Package: firefox*" | sudo tee -a /etc/apt/preferences.d/mozillateamppa
echo "Pin: release o=LP-PPA-mozillateam" | sudo tee -a /etc/apt/preferences.d/mozillateamppa
echo "Pin-Priority: 501" | sudo tee -a /etc/apt/preferences.d/mozillateamppa
echo
sudo apt update
echo
echo "Ya hemos terminado"
echo